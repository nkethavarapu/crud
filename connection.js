var mysql = require("mysql");

var con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "india2001",
  database: "education",
});

con.connect(function (err) {
  if (err) throw err;
  console.log("Connected!");

  con.query(sql, function (err, result) {
    if (err) throw err;
    console.log("query executed");
  });
});
var sql = "";

/**
 * this function creates a database
 * @param {string} db takes name of database
 *
 */
const createDb = (db) => {
  return `CREATE DATABASE ${db}`;
};
/**
 * this method is used to create table
 * @param {*} tbname name of the table
 * @param {*} tbSchema array that takes column name and its type respectively
 *
 */
const createTable = (
  tbname,
  [col1, type1, col2, type2, col3, type3, col4, type4]
) => {
  return ` CREATE TABLE ${tbname} (${col1} ${type1},${col2} ${type2},${col3} ${type3},${col4} ${type4})`;
};

/**
 * method to specify the database to be used
 * @param {string} dbName name of existing database
 *
 */

const useDB = (dbName) => {
  return `use ${dbName}`;
};
/**
 * this method inserts values into table
 * @param {*} tbname name of the table
 * @param {*} param1 array of colnames and values

 */
const createRecord = (
  tbname,
  [col1, value1, col2, value2, col3, value3, col4, value4]
) => {
  return `INSERT INTO ${tbname} (${col1}, ${col2}, ${col3}, ${col4}) VALUES ('${value1}',' ${value2}', '${value3}', '${value4}')`;
};
/**
 * this function reads the records of given table
 * @param {*} tableName name of the table
 *
 */

const readRecords = (tbname) => {
  return `SELECT * FROM ${tbname}`;
};
/**
 * deletes the table
 * @param {string} tableName name of the table
 * @returns
 */
const deleteTable = (tbname) => {
  return `DROP TABLE ${tbname}`;
};
/**
 * this is a function to delete database
 * @param {*} dbName name of database
 */

const deleteDB = (dbName) => {
  return `DROP DATABASE ${dbName}`;
};
const deleteRecords = (tbname, keyCol, keyVal) => {
  return `DELETE FROM ${tbname} WHERE ${keyCol}='${keyVal}'`;
};

function crud(x) {
  switch (x) {
    case 1:
      sql = createDb("education");
      break;
    case 2:
      sql = createTable("schools", [
        "name",
        "varchar(25)",
        "location",
        "varchar(25)",
        "id",
        "int(25)",
        "foundedYear",
        "int(25)",
      ]);
      break;
    case 3:
      sql = useDB("education");
      break;
    case 4:
      sql = createRecord("universities", [
        "name",
        "githam",
        "foundedYear",
        "1981",
        "Location",
        "Visakhapatnam",
        "collegeid",
        "1909",
      ]);
      break;
    case 5:
      sql = readRecords("universities");
      break;
    case 6:
      sql = deleteTable("schools");
      break;
    case 7:
      sql = deleteRecords("universities", "collegeid", "1909");
      break;

    case 8:
      sql = deleteDB("education");
      break;
  }
}
crud(1);
